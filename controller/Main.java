package controller;

import java.sql.SQLException;
import java.util.logging.Logger;

import bll.ClientBLL;
import model.Client;
import view.ClientGUI;
import view.MainGUI;

public class Main extends ClientGUI{
    protected static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws SQLException {

        ClientBLL clientBLL = new ClientBLL();
       

        MainGUI g= new MainGUI();
        g.main(args);


    }



}
