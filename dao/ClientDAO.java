package dao;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (id, name, age,email)"
			+ " VALUES (?,?,?,?)";
	private static final String deleteStatementString = "INSERT INTO client (id, name, age,email)"
			+ " VALUES (?,?,?,?)";
	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, client.getId());
			insertStatement.setString(2, client.getName());
			insertStatement.setInt(3, client.getVarsta());
			insertStatement.setString(4, client.getEmail());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static void update(Client client, int id) {
		Connection dbConnection = ConnectionFactory.getConnection();

		try 
		{  
			PreparedStatement st = dbConnection.prepareStatement("UPDATE client SET name = ?, age = ?, email = ? WHERE id= ?");
			
			st.setString(1,client.getName());
			st.setString(2,String.valueOf(client.getVarsta()));
			st.setString(3,client.getEmail());
			st.setString(4,String.valueOf(id));
			
			st.executeUpdate(); 

		}
		catch(Exception e)
		{
			System.out.println(e);
			;
		}
	}
	public static void detele(String name) {
		Connection dbConnection = ConnectionFactory.getConnection();

		try 
		{  
			PreparedStatement st = dbConnection.prepareStatement("DELETE FROM client WHERE name = ?");
			st.setString(1,name);
			st.executeUpdate(); 

		}
		catch(Exception e)
		{
			System.out.println(e);
			;
		}
	}
}
