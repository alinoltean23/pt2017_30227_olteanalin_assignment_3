package dao;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.OrderConnectionFactory;
import model.Order;

public class OrderDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO orders (id, clientid, produsid)"
			+ " VALUES (?,?,?)";
	private static final String deleteStatementString = "INSERT INTO orders (id, clientid, produsid)"
			+ " VALUES (?,?,?)";
	public static int insert(Order order) {
		Connection dbConnection = OrderConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getId());
			insertStatement.setInt(2, order.getClientid());
			insertStatement.setInt(3, order.getProdusid());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			OrderConnectionFactory.close(insertStatement);
			OrderConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static void update(Order order, int id) {
		Connection dbConnection = OrderConnectionFactory.getConnection();

		try 
		{  
			PreparedStatement st = dbConnection.prepareStatement("UPDATE orders SET clientid = ?, produsid = ? WHERE id= ?");
			
			st.setString(1,String.valueOf(order.getClientid()));
			st.setString(2,String.valueOf(order.getProdusid()));
			st.setString(3,String.valueOf(id));
			
			st.executeUpdate(); 

		}
		catch(Exception e)
		{
			System.out.println(e);
			;
		}
	}
	public static void detele(int id) {
		Connection dbConnection = OrderConnectionFactory.getConnection();

		try 
		{  
			PreparedStatement st = dbConnection.prepareStatement("DELETE FROM orders WHERE id = ?");
			st.setString(1,String.valueOf(id));
			st.executeUpdate(); 
		}
		catch(Exception e)
		{
			System.out.println(e);
			;
		}
	}
}
