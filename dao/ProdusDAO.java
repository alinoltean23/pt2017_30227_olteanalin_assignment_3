package dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ProdusConnectionFactory;
import model.Produs;

public class ProdusDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProdusDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO produs (id, nume, cantitate,dimensiune)"
			+ " VALUES (?,?,?,?)";
	private static final String deleteStatementString = "INSERT INTO produs (id, nume, cantitate,dimensiune)"
			+ " VALUES (?,?,?,?)";
	public static int insert(Produs produs) {
		Connection dbConnection = ProdusConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, produs.getId());
			insertStatement.setString(2, produs.getName());
			insertStatement.setInt(3, produs.getCantitate());
			insertStatement.setInt(4, produs.getDimensiune());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			ProdusConnectionFactory.close(insertStatement);
			ProdusConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static void update(Produs produs, int id) {
		Connection dbConnection = ProdusConnectionFactory.getConnection();

		try 
		{  
			PreparedStatement st = dbConnection.prepareStatement("UPDATE produs SET nume = ?, cantitate = ?, dimensiune = ? WHERE id= ?");
			
			st.setString(1,produs.getName());
			st.setString(2,String.valueOf(produs.getCantitate()));
			st.setString(3,String.valueOf(produs.getDimensiune()));
			st.setString(4,String.valueOf(id));
			
			st.executeUpdate(); 

		}
		catch(Exception e)
		{
			System.out.println(e);
			;
		}
	}
	public static void detele(String name) {
		Connection dbConnection = ProdusConnectionFactory.getConnection();

		try 
		{  
			PreparedStatement st = dbConnection.prepareStatement("DELETE FROM produs WHERE nume = ?");
			st.setString(1,name);
			st.executeUpdate(); 

		}
		catch(Exception e)
		{
			System.out.println(e);
			;
		}
	}
}
