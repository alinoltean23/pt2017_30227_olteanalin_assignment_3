package dao;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.AngajatiConnectionFactory;
import model.Angajati;

public class AngajatiDAO {

	protected static final Logger LOGGER = Logger.getLogger(AngajatiDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO angajati (id, name, age,email)"
			+ " VALUES (?,?,?,?)";
	private static final String deleteStatementString = "INSERT INTO angajati (id, name, age,email)"
			+ " VALUES (?,?,?,?)";
	public static int insert(Angajati angajati) {
		Connection dbConnection = AngajatiConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, angajati.getId());
			insertStatement.setString(2, angajati.getName());
			insertStatement.setInt(3, angajati.getVarsta());
			insertStatement.setString(4, angajati.getEmail());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			AngajatiConnectionFactory.close(insertStatement);
			AngajatiConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static void update(Angajati angajati, int id) {
		Connection dbConnection = AngajatiConnectionFactory.getConnection();

		try 
		{  
			PreparedStatement st = dbConnection.prepareStatement("UPDATE angajati SET name = ?, age = ?, email = ? WHERE id= ?");
			
			st.setString(1,angajati.getName());
			st.setString(2,String.valueOf(angajati.getVarsta()));
			st.setString(3,angajati.getEmail());
			st.setString(4,String.valueOf(id));
			
			st.executeUpdate(); 

		}
		catch(Exception e)
		{
			System.out.println(e);
			;
		}
	}
	public static void detele(String name) {
		Connection dbConnection = AngajatiConnectionFactory.getConnection();

		try 
		{  
			PreparedStatement st = dbConnection.prepareStatement("DELETE FROM angajati WHERE name = ?");
			st.setString(1,name);
			st.executeUpdate(); 

		}
		catch(Exception e)
		{
			System.out.println(e);
			;
		}
	}
}
