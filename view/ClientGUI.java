package view;
import java.sql.*;

import javax.swing.*;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import connection.ConnectionFactory;
import dao.ClientDAO;
import model.Client;
import net.proteanit.sql.DbUtils;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Scrollbar;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class ClientGUI {

	private JFrame frame;
	private JTextField ID;
	private JTextField Nume;
	private JTextField Varsta;
	private JTextField Email;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void ClientWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientGUI window = new ClientGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection dbConnection = ConnectionFactory.getConnection();
	/**
	 * Create the application.
	 */
	public ClientGUI() {
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 1378, 1240);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lbID = new JLabel("ID");
		lbID.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbID.setBounds(84, 274, 237, 33);
		frame.getContentPane().add(lbID);

		JLabel lbNume = new JLabel("Nume");
		lbNume.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbNume.setBounds(84, 354, 237, 33);
		frame.getContentPane().add(lbNume);

		JLabel lbVarsta = new JLabel("Varsta");
		lbVarsta.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbVarsta.setBounds(84, 449, 237, 33);
		frame.getContentPane().add(lbVarsta);

		JLabel lbEmail = new JLabel("Email");
		lbEmail.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbEmail.setBounds(84, 545, 237, 33);
		frame.getContentPane().add(lbEmail);

		JLabel lbClient = new JLabel("Client");
		lbClient.setHorizontalAlignment(SwingConstants.CENTER);
		lbClient.setFont(new Font("Tahoma", Font.PLAIN, 50));
		lbClient.setBounds(149, 63, 455, 121);
		frame.getContentPane().add(lbClient);

		ID = new JTextField();
		ID.setFont(new Font("Tahoma", Font.PLAIN, 35));
		ID.setBounds(261, 271, 236, 39);
		frame.getContentPane().add(ID);
		ID.setColumns(10);

		Nume = new JTextField();
		Nume.setFont(new Font("Tahoma", Font.PLAIN, 35));
		Nume.setBounds(261, 351, 236, 39);
		frame.getContentPane().add(Nume);
		Nume.setColumns(10);

		Varsta = new JTextField();
		Varsta.setFont(new Font("Tahoma", Font.PLAIN, 35));
		Varsta.setBounds(261, 446, 236, 39);
		frame.getContentPane().add(Varsta);
		Varsta.setColumns(10);

		Email = new JTextField();
		Email.setFont(new Font("Tahoma", Font.PLAIN, 35));
		Email.setBounds(261, 542, 236, 39);
		frame.getContentPane().add(Email);
		Email.setColumns(10);

		JButton btnAdauga = new JButton("Adauga");
		btnAdauga.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnAdauga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adaugaclient();
				try {
					String query="select * from client";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btnAdauga.setBounds(197, 689, 171, 41);
		frame.getContentPane().add(btnAdauga);

		JButton btdEdit = new JButton("Edit");
		btdEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateContact();
				try {
					String query="select * from client";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btdEdit.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btdEdit.setBounds(197, 817, 171, 41);
		frame.getContentPane().add(btdEdit);

		JButton btnSterge = new JButton("Sterge");
		btnSterge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stergeClient();
				try {
					String query="select * from client";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btnSterge.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnSterge.setBounds(197, 945, 171, 41);
		frame.getContentPane().add(btnSterge);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(551, 274, 745, 712);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 20));
		scrollPane.setViewportView(table);
		
		
		JButton btnLoad = new JButton("View Table");
		btnLoad.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String query="select * from client";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnLoad.setBounds(1071, 1016, 226, 41);
		frame.getContentPane().add(btnLoad);
	}
	public void adaugaclient(){
		int id_nou;
		String nume_nou;
		int varsta_nou;
		String email_nou;

		id_nou=Integer.parseInt(ID.getText());
		nume_nou=Nume.getText();
		varsta_nou=Integer.parseInt(Varsta.getText());
		email_nou= Email.getText();
		Client client_nou= new Client(id_nou,nume_nou, varsta_nou,email_nou);
		ClientDAO.insert(client_nou);

	}
	public void stergeClient(){
		String nume_sters;
		nume_sters=Nume.getText();
		ClientDAO.detele(nume_sters);
	}
	public void updateContact(){
		int id_nou;
		String nume_nou;
		int varsta_nou;
		String email_nou;

		id_nou=Integer.parseInt(ID.getText());
		nume_nou=Nume.getText();
		varsta_nou=Integer.parseInt(Varsta.getText());
		email_nou= Email.getText();
		Client client_nou= new Client(id_nou,nume_nou, varsta_nou,email_nou);
		ClientDAO.update(client_nou, id_nou);
	}
}
