package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class MainGUI extends ClientGUI{

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1378, 1240);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton ClientPopUp = new JButton("Client");
		ClientPopUp.setFont(new Font("Tahoma", Font.PLAIN, 50));
		ClientPopUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientGUI client= new ClientGUI();
				//frame.dispose();
				client.ClientWindow();
			}
		});
		
		ClientPopUp.setBounds(164, 527, 206, 78);
		frame.getContentPane().add(ClientPopUp);
		
		JButton ComandaPopUp = new JButton("Comanda");
		ComandaPopUp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				OrderGUI order= new OrderGUI();
				//frame.dispose();
				order.OrderWindow();
			}
		});
		ComandaPopUp.setFont(new Font("Tahoma", Font.PLAIN, 50));
		ComandaPopUp.setBounds(505, 527, 306, 78);
		frame.getContentPane().add(ComandaPopUp);
		
		JButton ProdusPopUp = new JButton("Produs");
		ProdusPopUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ProductGUI product= new ProductGUI();
				product.ProductWindow();
			}
		});
		ProdusPopUp.setFont(new Font("Tahoma", Font.PLAIN, 50));
		ProdusPopUp.setBounds(960, 527, 206, 78);
		frame.getContentPane().add(ProdusPopUp);
		
		JButton btnAngajati = new JButton("Angajati");
		btnAngajati.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AngajatiGUI angajati= new AngajatiGUI();
				angajati.AngajatiWindow();
			}
		});
		btnAngajati.setFont(new Font("Tahoma", Font.PLAIN, 50));
		btnAngajati.setBounds(505, 688, 306, 78);
		frame.getContentPane().add(btnAngajati);
	
	}
}
