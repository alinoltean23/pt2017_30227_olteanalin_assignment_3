package view;
import java.sql.*;

import javax.swing.*;
import java.awt.EventQueue;
import java.awt.Font;

import connection.ConnectionFactory;
import connection.ProdusConnectionFactory;
import dao.ProdusDAO;
import model.Produs;
import net.proteanit.sql.DbUtils;

import java.awt.Scrollbar;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class ProductGUI {

	private JFrame frame;
	private JTextField ID;
	private JTextField Nume;
	private JTextField Cantitate;
	private JTextField Dimensiune;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void ProductWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProductGUI window = new ProductGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection dbConnection = ConnectionFactory.getConnection();
	/**
	 * Create the application.
	 */
	public ProductGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1378, 1240);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lbID = new JLabel("ID");
		lbID.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbID.setBounds(84, 274, 237, 33);
		frame.getContentPane().add(lbID);

		JLabel lbNume = new JLabel("Nume");
		lbNume.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbNume.setBounds(84, 354, 237, 33);
		frame.getContentPane().add(lbNume);

		JLabel lbCantitate = new JLabel("Cantitate");
		lbCantitate.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbCantitate.setBounds(84, 449, 237, 33);
		frame.getContentPane().add(lbCantitate);

		JLabel lbDimensiune = new JLabel("Dimensiune");
		lbDimensiune.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbDimensiune.setBounds(84, 545, 237, 33);
		frame.getContentPane().add(lbDimensiune);

		JLabel lbProdus = new JLabel("Produs");
		lbProdus.setHorizontalAlignment(SwingConstants.CENTER);
		lbProdus.setFont(new Font("Tahoma", Font.PLAIN, 50));
		lbProdus.setBounds(149, 63, 455, 121);
		frame.getContentPane().add(lbProdus);

		ID = new JTextField();
		ID.setFont(new Font("Tahoma", Font.PLAIN, 35));
		ID.setBounds(261, 271, 236, 39);
		frame.getContentPane().add(ID);
		ID.setColumns(10);

		Nume = new JTextField();
		Nume.setFont(new Font("Tahoma", Font.PLAIN, 35));
		Nume.setBounds(261, 351, 236, 39);
		frame.getContentPane().add(Nume);
		Nume.setColumns(10);

		Cantitate = new JTextField();
		Cantitate.setFont(new Font("Tahoma", Font.PLAIN, 35));
		Cantitate.setBounds(261, 446, 236, 39);
		frame.getContentPane().add(Cantitate);
		Cantitate.setColumns(10);

		Dimensiune = new JTextField();
		Dimensiune.setFont(new Font("Tahoma", Font.PLAIN, 35));
		Dimensiune.setBounds(261, 542, 236, 39);
		frame.getContentPane().add(Dimensiune);
		Dimensiune.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(551, 274, 745, 712);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 20));
		scrollPane.setViewportView(table);

		JButton btnAdauga = new JButton("Adauga");
		btnAdauga.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnAdauga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adaugaprodus();
				try {
					String query="select * from produs";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btnAdauga.setBounds(197, 689, 171, 41);
		frame.getContentPane().add(btnAdauga);

		JButton btdEdit = new JButton("Edit");
		btdEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateProdus();
				try {
					String query="select * from produs";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btdEdit.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btdEdit.setBounds(197, 817, 171, 41);
		frame.getContentPane().add(btdEdit);

		JButton btnSterge = new JButton("Sterge");
		btnSterge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stergeProdus();
				try {
					String query="select * from produs";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btnSterge.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnSterge.setBounds(197, 945, 171, 41);
		frame.getContentPane().add(btnSterge);
		
		JButton btnLoad = new JButton("View Table");
		btnLoad.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String query="select * from produs";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnLoad.setBounds(1071, 1016, 226, 41);
		frame.getContentPane().add(btnLoad);
	}
	
	public void adaugaprodus(){
		int id_nou;
		String nume_nou;
		int cantitate_nou;
		int dimensiune_nou;

		id_nou=Integer.parseInt(ID.getText());
		nume_nou=Nume.getText();
		cantitate_nou=Integer.parseInt(Cantitate.getText());
		dimensiune_nou=Integer.parseInt(Dimensiune.getText());
		Produs produs_nou= new Produs(id_nou,nume_nou, cantitate_nou,dimensiune_nou);
		ProdusDAO.insert(produs_nou);

	}
	public void stergeProdus(){
		String nume_sters;
		nume_sters=Nume.getText();
		ProdusDAO.detele(nume_sters);
	}
	public void updateProdus(){
		int id_nou;
		String nume_nou;
		int cantitate_nou;
		int dimensiune_nou;

		id_nou=Integer.parseInt(ID.getText());
		nume_nou=Nume.getText();
		cantitate_nou=Integer.parseInt(Cantitate.getText());
		dimensiune_nou=Integer.parseInt(Dimensiune.getText());
		Produs produs_nou= new Produs(id_nou,nume_nou, cantitate_nou,dimensiune_nou);
		ProdusDAO.update(produs_nou, id_nou);
	}
	
}
