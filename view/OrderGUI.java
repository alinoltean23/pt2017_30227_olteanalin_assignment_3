package view;

import java.awt.EventQueue;
import java.sql.*;

import javax.swing.*;

import connection.ConnectionFactory;
import dao.OrderDAO;
import model.Order;
import net.proteanit.sql.DbUtils;

import java.awt.Font;
import java.awt.Scrollbar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrderGUI {

	private JFrame frame;
	private JTextField ID;
	private JTextField Clientid;
	private JTextField Produsid;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void OrderWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderGUI window = new OrderGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	Connection dbConnection = ConnectionFactory.getConnection();

	/**
	 * Create the application.
	 */
	public OrderGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1378, 1240);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lbID = new JLabel("ID");
		lbID.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbID.setBounds(84, 274, 237, 33);
		frame.getContentPane().add(lbID);
		
		JLabel lbClientid = new JLabel("ClientID");
		lbClientid.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbClientid.setBounds(84, 354, 237, 33);
		frame.getContentPane().add(lbClientid);
		
		JLabel lbProdusid = new JLabel("ProdusID");
		lbProdusid.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lbProdusid.setBounds(84, 449, 237, 33);
		frame.getContentPane().add(lbProdusid);
		
		JLabel lbClient = new JLabel("Order");
		lbClient.setHorizontalAlignment(SwingConstants.CENTER);
		lbClient.setFont(new Font("Tahoma", Font.PLAIN, 50));
		lbClient.setBounds(149, 63, 455, 121);
		frame.getContentPane().add(lbClient);
		
		ID = new JTextField();
		ID.setFont(new Font("Tahoma", Font.PLAIN, 35));
		ID.setBounds(261, 271, 236, 39);
		frame.getContentPane().add(ID);
		ID.setColumns(10);
		
		Clientid = new JTextField();
		Clientid.setFont(new Font("Tahoma", Font.PLAIN, 35));
		Clientid.setBounds(261, 351, 236, 39);
		frame.getContentPane().add(Clientid);
		Clientid.setColumns(10);
		
		Produsid = new JTextField();
		Produsid.setFont(new Font("Tahoma", Font.PLAIN, 35));
		Produsid.setBounds(261, 446, 236, 39);
		frame.getContentPane().add(Produsid);
		Produsid.setColumns(10);
		
		JButton btnAdauga = new JButton("Adauga");
		btnAdauga.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnAdauga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adaugaComanda();
				try {
					String query="select * from orders";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnAdauga.setBounds(197, 689, 171, 41);
		frame.getContentPane().add(btnAdauga);
		
		JButton btdEdit = new JButton("Edit");
		btdEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateComanda();
				try {
					String query="select * from orders";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btdEdit.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btdEdit.setBounds(197, 817, 171, 41);
		frame.getContentPane().add(btdEdit);
		
		JButton btnSterge = new JButton("Sterge");
		btnSterge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stergeComanda();
			}
		});
		btnSterge.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnSterge.setBounds(197, 945, 171, 41);
		frame.getContentPane().add(btnSterge);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(551, 274, 745, 712);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 20));
		scrollPane.setViewportView(table);
		
		
		JButton btnLoad = new JButton("View Table");
		btnLoad.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String query="SELECT client.name, produs.nume, produs.cantitate FROM orders JOIN client on orders.clientid=client.id JOIN produs ON orders.produsid=produs.id";
					PreparedStatement pst = dbConnection.prepareStatement(query);
					ResultSet rs= pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnLoad.setBounds(1071, 1016, 226, 41);
		frame.getContentPane().add(btnLoad);

	}
	public void adaugaComanda(){
		int id_nou;
		int clientid_nou;
		int produsid_nou;

		id_nou=Integer.parseInt(ID.getText());
		clientid_nou=Integer.parseInt(Clientid.getText());
		produsid_nou=Integer.parseInt(Produsid.getText());
		Order order_nou= new Order(id_nou,clientid_nou, produsid_nou);
		OrderDAO.insert(order_nou);

	}
	public void updateComanda(){
		int id_nou;
		int clientid_nou;
		int produsid_nou;

		id_nou=Integer.parseInt(ID.getText());
		clientid_nou=Integer.parseInt(Clientid.getText());
		produsid_nou=Integer.parseInt(Produsid.getText());
		Order order_nou= new Order(id_nou,clientid_nou, produsid_nou);
		OrderDAO.update(order_nou, id_nou);
	}
	public void stergeComanda(){
		int id_sters;
		id_sters=Integer.parseInt(ID.getText());
		OrderDAO.detele(id_sters);
	}
//	fileName = "Receipt-" + selectedOrder.getOrderDate() + "-Client-" + client.getFirstName() + client.getLastName() + ".txt";
//    PrintWriter writer = new PrintWriter(fileName, "UTF-8");
//    writer.println("Order for client " + client.getFirstName() + " " + client.getLastName() + " : ");
//    int i = 1;
//    for (Product product : products){
//        double price = product.getPrice();
//        writer.println("\tProduct " + i + " : " + product.getName() + "...." + price + " per unit");
//        int quantity = orderLinks.get(i-1).getQuantity();
//        writer.println("\t\tQuantity : " + quantity + " | Final price : " + price*quantity);
//        i++;
//    }
//    writer.close();

}
