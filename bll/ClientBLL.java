package bll;

import dao.ClientDAO;

import model.Client;

/**
 * Created by alino on 4/18/2017.
 */
public class ClientBLL {
    public int insertClient(Client client) {
        return ClientDAO.insert(client);
    }
}
