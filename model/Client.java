package model;


public class Client {
    private int id;
    private String nume;
    private int varsta;
    private String email;

    public Client(int id, String name, int varsta, String email) {
        super();
        this.id = id;
        this.nume = name;
        this.varsta = varsta;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return nume;
    }

    public void setName(String name) {
        this.nume = name;
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", name=" + nume + ", phoneNumber=" + varsta + ", email=" + email + ", age="+ "]";
    }

}
