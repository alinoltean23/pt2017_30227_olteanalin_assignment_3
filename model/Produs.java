package model;


public class Produs {
    private int id;
    private String nume;
    private int cantitate;
    private int dimensiune;

    public Produs(int id, String name, int cantitate, int dimensiune) {
        super();
        this.id = id;
        this.nume = name;
        this.cantitate=cantitate;
        this.dimensiune = dimensiune;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return nume;
    }

    public void setName(String name) {
        this.nume = name;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public int getDimensiune() {
        return dimensiune;
    }

    public void setDimensiune (int dimensiune) {
        this.dimensiune = dimensiune;
    }

    @Override
    public String toString() {
        return "Product [id=" + id + ", name=" + nume + ", cantitate=" + cantitate + ", dimesiune=" + dimensiune + ", age="+ "]";
    }

}
