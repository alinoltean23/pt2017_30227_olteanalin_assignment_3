package model;


public class Order {
    private int id;
    private int clientid;
    private int produsid;

    public Order(int id,int clientid,int produsid) {
        super();
        this.id = id;
        this.clientid=clientid;
        this.produsid=produsid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientid() {
        return clientid;
    }

    public void setClientid(int clientid) {
        this.clientid =clientid;
    }

    public int getProdusid() {
        return produsid;
    }

    public void setProdusid(int produsid) {
        this.produsid = produsid;
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", clientid=" + clientid + ", produsid=" + produsid + "]";
    }

}
